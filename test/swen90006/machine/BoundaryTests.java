package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  //Any method annotated with "@Before" will be executed before each test,
  //allowing the tester to set up some shared resources.
  @Before public void setUp()
  {
  }

  //Any method annotated with "@After" will be executed after each test,
  //allowing the tester to release any shared resources used in the setup.
  @After public void tearDown()
  {
  }

  //Any method annotation with "@Test" is executed as a test.
  
  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void ec3Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("jmp -1");
    list.add("ret R0");	
    Machine m = new Machine();
    m.execute(list);
  }

  @Test public void ec4Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("jmp 1");
    list.add("ret R0");	
    Machine m = new Machine();
    assertEquals(m.execute(list), 0);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ec5Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("jmp 65536");
    list.add("ret R0");	
    Machine m = new Machine();
    m.execute(list);
  } 
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ec6Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("ret R32");	
    Machine m = new Machine();
    m.execute(list);
  }
  
  @Test public void ec7Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("ret R0");	
    Machine m = new Machine();
    assertEquals(m.execute(list), 0);
  }
  
  @Test public void ec8Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("mov R1 65535");
    list.add("ret R1");	
    Machine m = new Machine();
    assertEquals(m.execute(list), 65535);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ec9Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("mov R1 65536");
    list.add("ret R1");	
    Machine m = new Machine();
    m.execute(list);
  }
 
  
  @Test(expected = swen90006.machine.NoReturnValueException.class)
  public void ec11Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("jz R1 3");
    list.add("ret R1");	
    Machine m = new Machine();
    m.execute(list);
  }
  
  @Test public void ec12Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("jz R1 0");
    list.add("ret R1");	
    Machine m = new Machine();
    assertEquals(m.execute(list), 0);
  }
  
  @Test public void ec13Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("mov R0 1");
	list.add("ldr R0 R1 0");
    list.add("ret R0");	
    Machine m = new Machine();
    assertEquals(m.execute(list), 0);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ec14Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("mov R0 1");
	list.add("ldr R0 R1 65536");
    list.add("ret R0");	
    Machine m = new Machine();
    m.execute(list);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ec15Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("mov R1 65536");
	list.add("ldr R0 R1 0");
    list.add("ret R0");	
    Machine m = new Machine();
    (m.execute(list);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ec16Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
	list.add("ldr R0 R32 0");
    list.add("ret R0");	
    Machine m = new Machine();
    m.execute(list);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ec17Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
	list.add("str R0 0 R32");
    list.add("ret R0");	
    Machine m = new Machine();
    m.execute(list);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ec18Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("mov R1 65536");
	list.add("str R1 0 R0");
    list.add("ret R0");	
    Machine m = new Machine();
    m.execute(list);
  }
  
  @Test public void ec20Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();
    list.add("mov R1 0");
    list.add("mov R2 65535");    
	list.add("add R0 R1 R2");
    list.add("ret R0");	
    Machine m = new Machine();
    assertEquals(m.execute(list), 65535);
  }
  
  @Test(expected = swen90006.machine.InvalidInstructionException.class)
  public void ec21Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();   
	list.add("add R0 R1 R32");
    list.add("ret R0");	
    Machine m = new Machine();
    m.execute(list);
  }
  
  @Test public void ec22Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();   
    list.add("mov R1 65535");
    list.add("mov R2 1");
	list.add("div R0 R1 R2");
    list.add("ret R0");	
    Machine m = new Machine();
    assertEquals(m.execute(list), 65535);
  }
  
  @Test public void ec23Test()
  {
    //the assertEquals method used to check whether two values are
    //equal, using the equals method
	List<String> list = new ArrayList<String>();   
    list.add("mov R1 1");
    list.add("mov R2 0");
	list.add("div R0 R1 R2");
    list.add("ret R0");	
    Machine m = new Machine();
    assertEquals(m.execute(list), 0);
  }
  
  //Test test opens a file and executes the machine
  @Test public void aFileOpenTest()
  {
    final List<String> lines = readInstructions("examples/array.s");
    Machine m = new Machine();
    assertEquals(m.execute(lines), 45);
  }
  
  //To test an exception, specify the expected exception after the @Test
  @Test(expected = java.io.IOException.class) 
    public void anExceptionTest()
    throws Throwable
  {
    throw new java.io.IOException();
  }

  //This test should fail.
  //To provide additional feedback when a test fails, an error message
  //can be included
  @Test public void aFailedTest()
  {
    //include a message for better feedback
    final int expected = 2;
    final int actual = 1 + 2;
    assertEquals("Some failure message", expected, actual);
  }

  //Read in a file containing a program and convert into a list of
  //string instructions
  private List<String> readInstructions(String file)
  {
    Charset charset = Charset.forName("UTF-8");
    List<String> lines = null;
    try {
      lines = Files.readAllLines(FileSystems.getDefault().getPath(file), charset);
    }
    catch (Exception e){
      System.err.println("Invalid input file! (stacktrace follows)");
      e.printStackTrace(System.err);
      System.exit(1);
    }
    return lines;
  }
}
